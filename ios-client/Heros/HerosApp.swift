//
//  HerosApp.swift
//  Heros
//
//  Created by Paul Plaquette on 23/04/2021.
//

import SwiftUI

@main
struct HeroesApp: App {
    @StateObject private var heroesData =  HeroesDataModel()
    var body: some Scene {
        WindowGroup {
            HeroesView()
                .environmentObject(heroesData)
        }
    }
}
