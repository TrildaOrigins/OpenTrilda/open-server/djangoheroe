//
//  HerosDataModel.swift
//  Heros
//
//  Created by Paul Plaquette on 23/04/2021.
//


import Foundation
import Combine



final class HeroesDataModel : ObservableObject {

    //@Published var heroes : [Heroe] = [Heroe]()
    @Published var heroes : [Heroe] = load("heroes.json")
}

func load<T:Decodable>(_ filename: String) -> T {

    let data: Data

    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
    else{
        fatalError("Couldn't find \(filename) in main module.")
    }

    do {
        print("aqui1")
        data = try Data(contentsOf: file)
        print(data.description)
    } catch {
        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
    }

    do {
        print("aqui2")
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    }
    catch {
        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
}



struct Response: Codable, Hashable {
    var heroes: [Heroe]
}
